import employees.Employee;
import employees.EmployeeFixedSalary;
import employees.EmployeeHourlyWages;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class Company {
    private static final String SELECT_ALL = "SELECT * FROM EMPLOYEES";

    private DataBase db;

    private ArrayList<Employee> employees = new ArrayList<Employee>();

    public Company(String nameDataBase) throws SQLException {
        db = new DataBase(nameDataBase);
        ResultSet rs = db.executeQuery(SELECT_ALL);

        Employee employee = null;
        while ( rs.next() ) {
            String name = rs.getString("name");
            String surname = rs.getString("surname");
            Date birth = rs.getDate("birth");

            double fixedSalary = rs.getDouble("fixedsalary");

            if (rs.wasNull()){
                double hourlyWages = rs.getDouble("hourlyWages");
                employee = new EmployeeHourlyWages(name, surname, birth, hourlyWages);
            }
            else
                employee = new EmployeeFixedSalary(name, surname, birth, fixedSalary);
            employees.add(employee);
        }
    }

    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    public void deleteEmployee(Employee employee) {
        employees.remove(employee);
    }

    public void close() {
        for (Employee employee : employees) {

        }
    }

    public void printAllEmployees() {
        printAllEmployees(this.employees);
    }

    public void printAllEmployees(List<Employee> employees) {
        System.out.printf("%-20s|%-20s| %-10s |%-10s\n", "Name", "Surname", "Birth", "Avg salary");
        for (Employee employee : employees) {
            System.out.printf("%-20s|%-20s| %tF |%.2f\n", employee.getName(), employee.getSurname(), employee.getBirth(), employee.getAverageSalary());
        }
    }

    private List<Employee> sortBy(Comparator<Employee> comparator) {
        List<Employee> sortedEmployees = new ArrayList<Employee>(employees);
        Collections.sort(sortedEmployees, comparator);
        return sortedEmployees;
    }

    public List<Employee> sortByName() {
        return sortBy(new Comparator<Employee>() {
            @Override
            public int compare(Employee o1, Employee o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
    }

    public List<Employee> sortBySurname() {
        return sortBy(new Comparator<Employee>() {
            @Override
            public int compare(Employee o1, Employee o2) {
                return o1.getSurname().compareTo(o2.getSurname());
            }
        });
    }

    public List<Employee> sortByBirth() {
        return sortBy(new Comparator<Employee>() {
            @Override
            public int compare(Employee o1, Employee o2) {
                return o1.getBirth().compareTo(o2.getBirth());
            }
        });
    }

    public List<Employee> sortByAvgSalary() {
        return sortBy(new Comparator<Employee>() {
            @Override
            public int compare(Employee o1, Employee o2) {
                return o1.getAverageSalary().compareTo(o2.getAverageSalary());
            }
        });
    }

    public static void main(String[] args) throws SQLException {
        Company company = new Company("Company.db");
        company.printAllEmployees();
        System.out.println();
        company.printAllEmployees(company.sortByAvgSalary());
    }
}
