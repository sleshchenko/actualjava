import java.sql.*;

public class DataBase {
    private final static String DRIVER = "org.sqlite.JDBC";

    /**** Загрузка драйвера ****/
    static {
        try {
            Class.forName(DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Connection connection;

    public DataBase(String url) {
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:" + url);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private Statement getStatement() {
        Statement statement = null;
        try {
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return statement;
    }

    public ResultSet executeQuery(String query) {
        Statement statement = getStatement();
        ResultSet result = null;
        try {
            result = statement.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public int executeUpdate(String query) {
        Statement statement = getStatement();
        int countUpdate = 0;
        try {
            countUpdate = statement.executeUpdate(query);
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return countUpdate;
    }

    public boolean execute(String query) {
        Statement statement = null;
        boolean res = false;
        try {
            statement = getStatement();
            res = statement.execute(query);
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    public void close(){
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws SQLException {
        DataBase db = new DataBase("Company.db");

        db = new DataBase("Company.db");
//        db.executeUpdate("insert into employees('name', 'surname', 'birth', 'hourlywages') values('Jacob', 'Qaz', '1988-06-02', 1343.9)");
//        db.executeUpdate("insert into employees('name', 'surname', 'birth', 'hourlywages') values('Bob', 'Marly', '1989-04-03', 1200.9)");
//        db.executeUpdate("insert into employees('name', 'surname', 'birth', 'fixedsalary') values('Bruce', 'Li', '1970-10-09', 1900.4)");
//        db.executeUpdate("insert into employees('name', 'surname', 'birth', 'fixedsalary') values('Jeki', 'Jun', '1969-11-5', 2500.4)");



        //bd.execute("create table if not exists 'employees' ('id' INTEGER PRIMARY KEY AUTOINCREMENT, 'name' text, 'surname' text, 'birth' date, 'fixedsalary' double, 'hourlywages' double );");
    }

}
