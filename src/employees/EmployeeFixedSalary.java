package employees;

import java.util.Date;

public class EmployeeFixedSalary extends Employee {
    private double salary;

    public EmployeeFixedSalary(String name, String surname, Date birth, double salary) {
        super(name, surname, birth);
        this.salary = salary;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public Double getAverageSalary() {
        return salary;
    }
}
