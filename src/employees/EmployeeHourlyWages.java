package employees;

import java.util.Date;

public class EmployeeHourlyWages extends Employee {
    private double hourlyWages;

    public EmployeeHourlyWages(String name, String surname, Date birth, double hourlyWages) {
        super(name, surname, birth);
        this.hourlyWages = hourlyWages;
    }

    public double getHourlyWages() {
        return hourlyWages;
    }

    public void setHourlyWages(double hourlyWages) {
        this.hourlyWages = hourlyWages;
    }

    @Override
    public Double getAverageSalary() {
        return 20.8 * 8 * hourlyWages;
    }
}
